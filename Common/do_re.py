from Common.config import ReadConfig
import configparser
from Common.do_log import MyLog


class Context:
    """
    设定反射的类
    """


class DoRe:

    def do_match_repalce(self, data):
        """
        1.用正则表达式来匹配测试数据中的指定字符
        2.根据指定字符获取到配置文件中对应的值
        3.然后进行替换
        """

        pattern = '#(.*?)#'  # 正则表达式   匹配组当中多次或者最多一次单个字符

        while re.search(pattern, data):

            search = re.search(pattern, data)  # 从任意位置开始找，找第一个就返回Match object, 如果没有找None

            group = search.group(1)  # 拿到参数化的KEY

            try:  # 根据KEY取配置文件里面的值

                value = ReadConfig().get_value('data', group)

            except configparser.NoOptionError as e:  # 如果配置文件里面没有的时候，去do_re模块中类Context里面取

                if hasattr(Context, group):  # 判断类属性名是否在Context中

                    value = getattr(Context, group)  # 获取类属性值
                else:
                    MyLog(__name__).my_log().error('找不到参数化的值。报错：{}'.format(e))
                    raise e

            data = re.sub(pattern, value, data, count=1)  # 记得替换后的内容，继续用data接收,查找替换,count查找替换的次数

        return data

    def do_match(self, pattern, data):
        """
        :param pattern:
        表达式: (?<=A).*?(?=(B|$))  匹配AB之间的字符串
                A.*?B              匹配A开始B结束的匹配
        :param data: 需要匹配的数据（必须是字符串）
        :return: 返回匹配后的字符串
        """

        result = re.search(pattern, data).group()
        return result


if __name__ == '__main__':
    import re

bb = '{"status": 0, "msg": "success", "time": 1566614006, "sign": "12487843456456",\
         "data": {"userid": 564564564, "userType": 24, "pick": 1, "newreg": 0, "expires": "2019-09-01 14:46:56",\
                  "gameLogoUrl": "http://download.xunyou.com/gamelogo2014/",\
                  "store": [{"gameId": 23646, "gameName": "绝地求生-Steam", "gameType": "PC", "fouter": 0, "isp2p": 0},\
                            {"gameId": 1616, "gameName": "LOL英雄联盟", "gameType": "PC", "fouter": 0, "isp2p": 0}],\
                  "record": [{"23646": [\
                      {"deviceName": "华为", "deviceBrand": "huawei", "mac": ["14:B3:1F:03:40:DF", "00:0c:29:c5:45:1d"],\
                       "gameId": 23646, "gameName": "绝地求生", "gameType": "PC", "iconType": "路由器", "interfaceType": "有线",\
                       "acceleteTime": 0, "date": "2019-08-01 18:00:00", "averageDelay": 0, "packetLoss": 6,\
                       "stability": 0, "speedUp": 100},\
                      {"deviceName": "华为", "deviceBrand": "huawei", "mac": ["14:B3:1F:03:40:DF", "00:0c:29:c5:45:1d"],\
                       "gameId": 23646, "gameName": "绝地求生", "gameType": "PC", "iconType": "路由器", "interfaceType": "有线",\
                       "acceleteTime": 0, "date": "2019-08-01 18:00:00", "averageDelay": 0, "packetLoss": 6,\
                       "stability": 0, "speedUp": 100}]}, {"1616": [\
                      {"deviceName": "华为", "deviceBrand": "huawei", "mac": ["14:B3:1F:03:40:DF", "00:0c:29:c5:45:1d"],\
                       "gameId": 1616, "gameName": "LOL英雄联盟", "gameType": "PC", "iconType": "路由器",\
                       "interfaceType": "有线", "acceleteTime": 0, "date": "2019-08-01 18:00:00", "averageDelay": 0,\
                       "packetLoss": 6, "stability": 0, "speedUp": 100}]}]}}'

result = DoRe().do_match('(?<=userid": ).*?(?=(,|$))', bb)
print(result)
