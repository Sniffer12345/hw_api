import pytest

from Common.do_excel import DoExcel
from Common import contants
from Common.do_log import MyLog



class TestPickPackage:
    doexcel_ob = DoExcel(contants.case_file, 'PickPackage')
    log_ob = MyLog(__name__).my_log()

    @pytest.mark.parametrize('case', doexcel_ob.get_cases())
    def test_PickPackage(self, before_after_request, case):
        self.log_ob.debug('第 {} 条用例'.format(case.case_id))
        self.log_ob.debug('用例名称：{}'.format(case.case_name))
        resp = before_after_request.http_request(case.method, case.url, case.data)
        print(resp)
        result=resp.json()
        exp=eval(case.expected)
        assert exp['status'] ==result['status']
